= Pràctica servidor FTP

:encoding: utf-8
:lang: ca 
:toc: left
:!numbered:
//:teacher:

ifdef::teacher[]
== (Versió del professor):
endif::teacher[]

<<<

== Apartat 6: Configuració de la capa de xifratge SSL


En aquest apartat afegirem una capa de xifrat per sobre de la connexió FTP.


Aprofita la base teòrica vista a M17 i entrega:

ifndef::teacher[]
.Entregar
====
*Captura on es vegi el Filezilla connectat a _Fry_.*

image:Capturas_3/Screenshot_1.png[]
====
endif::teacher[]

ifndef::teacher[]
.Entregar
====
*Captura del Filezilla on es vegi alguna informació del certificat digital.*

image:Capturas_3/Screenshot_2.png[]
image:Capturas_3/Screenshot_3.png[]
image:Capturas_3/Screenshot_4.png[]
image:Capturas_3/Screenshot_5.png[]
====
endif::teacher[]

